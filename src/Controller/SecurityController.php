<?php

namespace App\Controller;
use App\Document\Account;
use App\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class SecurityController extends Controller
{
    public function login(Request $request)
    {
        $rep=$request->getContent();
        //$response = new JsonResponse(['status'=>$this->get('security.token_storage')->getToken()]);
         //return $response;
         $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        
  
        $serializer = new Serializer($normalizers, $encoders);

        $account = $serializer->deserialize($rep, Account::class, 'json');
        $authenticationSuccessHandler = $this->container->get('lexik_jwt_authentication.handler.authentication_success');
    
        return $authenticationSuccessHandler->handleAuthenticationSuccess($account);
    }
    /**
     * @Route("/security", name="security")
     */
    public function index()
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }
    /**
     * @Route("/register",name="register",methods={"POST"})
     */
    public function register(Request $request,UserPasswordEncoderInterface $encoder)
    {
        $rep= $request->getContent();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        
  
        $serializer = new Serializer($normalizers, $encoders);

        $account = $serializer->deserialize($rep, Account::class, 'json');
        $account->setPassword($encoder->encodePassword($account,$account->getPassword()));
        $user=$account->getUser();
        //dump($account);
        //dump($user);
        $account->setUser(new User());
        $account->getUser()->setFirstName($user["firstName"]);
        $account->getUser()->setLastName($user["lastName"]);
        $account->getUser()->setNumTel($user["numTel"]);
        //dump($account);

        $dm = $this->get('doctrine_mongodb')->getManager();

         $dm->persist($account);
         $dm->flush();
         $jwtManager = $this->container->get('lexik_jwt_authentication.jwt_manager');

    return new JsonResponse(['token' => $jwtManager->create($account)]);
    }
}
