<?php

namespace App\Controller;
use App\Document\Reponse;
use App\Document\Question;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Routing\Annotation\Route;


class ReponseController extends Controller
{
    private $serializer;
    public function __construct(SerializerInterface $serializer){
        $this->serializer=$serializer;
        }
    /**
     * @Route("/reponse", name="reponse",methods={"POST"})
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ReponseController.php',
        ]);
    }

    /**
     * 
     * @Route("api/reponses",methods={"POST"})
     * 
     * 
     */
    public function showAction(Request $request)
    {
      /*   $reponse = new Reponse();
        $reponse
            ->setLabel('mon api')
            
        ;
        $data = $this->serializer->serialize($reponse, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response; */
        $rep= $request->getContent();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        
  
        $serializer = new Serializer($normalizers, $encoders);

        $reponse = $serializer->deserialize($rep, Reponse::class, 'json');
        $repo = new Reponse();
        $repo->setLabel($reponse->getLabel()) ;
        $question = new Question();
        $question->setMessage("coucou");
        $repo->setQuestion($question);
        $question->addReponse($repo);

        //dump($reponse);
        

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($question);
        $dm->persist($repo);
        
        $dm->flush();
        $response = new JsonResponse(['status'=>'Ok']);

        return $response;
    

    }
    // …
    
    /**
     * @Rest\Post(
     *    path = "/api/reponse",
     *    name = "app_article_create"
     * )
     * @Rest\View(StatusCode = 201)
     * @ParamConverter("reponse", converter="fos_rest.request_body")
     */
    public function createAction(Reponse $reponse)
    {
        dump($reponse); die;
    }
    /**
     * @Route("api/reponses",methods={"GET"})
     */
    public function showAll(Request $request)
    {
        $dm=$this->get('doctrine_mongodb')->getManager();
        
        $reponses=$dm->getRepository(Reponse::class)->findAll();
       // dump($reponses);
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        
        
        $serializer = new Serializer($normalizers, $encoders);
        $reponse = new JsonResponse($this->serializer->serialize($reponses, 'json'));
        return $reponse;
        
    }
    /**
     * @Route("api/reponses/{id}",methods={"PUT"})
     */
    public function modifier($id)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $reponse=$dm->getRepository(Reponse::class)->find($id);
        $encoders = array(new JsonEncoder());
        
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $reponse->setLabel("gape");
        $dm->flush();
        $response = new JsonResponse(['status'=>'Ok']);
        return $response;
    }
}
