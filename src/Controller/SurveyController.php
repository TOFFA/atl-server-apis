<?php

namespace App\Controller;
use App\Document\Reponse;
use App\Document\Survey;
use Doctrine\Common\Collections\ArrayCollection;
use App\Document\Question;
use App\Document\Content;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class SurveyController extends Controller
{
    private $serializer;
    public function __construct(SerializerInterface $serializer){
        $this->serializer=$serializer;
        }
    /**
     * @Route("/reponse", name="reponse",methods={"POST"})
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ReponseController.php',
        ]);
    }
    /**
     * @Route("api/reponse", name="reponse",methods={"GET"})
     */
    public function jou()
    {
        dump($this->get('security.token_storage')->getToken()->getUser());
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ReponseController.php',
        ]);

        }

    /**
     * @Route("api/addsurvey",name="surveys",methods={"POST"})
     */
    public function addSurvey(Request $request)
    {
        $rep= $request->getContent();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        
  
        $serializer = new Serializer($normalizers, $encoders);

        $surv = $serializer->deserialize($rep,Survey::class,'json');
        $content = $surv->getContent();
        $surve = new Survey();
        $surve->setTitle($surv->getTitle());
        $surve->setContent(new Content());
        
        //cette boucle sera necessaire pour la serialisation 
        foreach($content["questions"] as $question)
        {
           $question1 = new Question();
           $question1->setMessage($question["message"]);
            $question1->setTypeReponse($question["typeReponse"]); 
          // dump($question["reponses"]);
           foreach($question["reponses"] as $reponse)
           {
               $rep = new Reponse();
               $rep->setLabel($reponse["label"]);
               $question1->addReponse($rep);
           }
           dump($question1);
           $surve->getContent()->addQuestions($question1);

           //$content->addQuestions($question1);
        }
        
        $surv->setContent($content);
        //$surv->setAuthor($this->get('security.token_storage')->getToken()->getUser());
        //dump($surve);
        //dump($surv);
        //dump($reponse);
        

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($surve);
        $dm->flush();
        $response = new JsonResponse(['status'=>'Ok']);

        return $response;
    }
    /**
     * @Route("api/surveys",methods={"GET"})
     */
    public function show(Request $request)
    {
        $dm=$this->get('doctrine_mongodb')->getManager();
        
        $survey= new Survey();
        $survey->setTitle("soClean!");
        $content=new Content();
        $question1=new Question();
        $reponse1=new Reponse();
        $reponse2=new Reponse(); 
        $question1->setMessage("savez vous que le togo est independant?");
        $reponse1->setLabel("oui");
        $reponse2->setLabel("non");
        $question1->addReponse($reponse1);
        $question1->addReponse($reponse2);
        $content->addQuestions($question1);
        $survey->setContent($content);
        
        //dump($survey);
       // dump($reponses);
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        
        
        $serializer = new Serializer($normalizers, $encoders);
        $reponse = new JsonResponse($this->serializer->serialize($survey, 'json'));
        return $reponse;
        
    }
    /**
     * @Route("api/getSurveyAnswer",methods={"POST"})
     */
    public function getSurveyAnswer(Request $request)
    {
       $rep = $request->getContent();
       $encoders = array(new JsonEncoder());
       $normalizers = array(new ObjectNormalizer());
       $serializer = new Serializer($normalizers,$encoders);
           
       /**Une participation est caractérisé par le croisement  d'un utilisateur et d'une réponse*/
       $participation =  $serializer->deserialize($rep,SurveyUser::class);
       
       $dm = $this->get('doctrine_mongodb')->getManager();
       $participation->setSurvey($dm->getRepository(Survey::class)->find($participation->getSurvey()->getId()));
       $participation->setUser($this->get('security.token_storage')->getToken()->getUser());
       dump($participation->getGivenAnswers());
       /* $answers = new ArrayCollection();
       foreach($answers as $answer)
       {
            
       } */


        
    }
    /**
     * @Route("api/surveys/stats/{{id}}",methods={"GET"})
     */
    public function stats()
    {
       $dm = $this->get('doctrine_mongodb')->getManager();
       $participations = new ArrayCollection();
       
       $participations = $dm->getRepository(SurveyUser::class)->findBySurveyId($id);
       
       ;
    }
    /**
     * @Route("api/test",methods={"GET"})
     * 
     */
    public function test()
    {
        $col =  array();
        
        $coordonnees = array (
            'prenom' => 'François',
            'nom' => 'Dupont',
            'adresse' => '3 Rue du Paradis',
            'ville' => 'Marseille');
            $coordonnees2 = array (
                'prenom' => 'François',
                'nom' => 'Dupont',
                'adresse' => '3 Rue du Paradis',
                'ville' => 'Marseille');
            $col[0]=$coordonnees;
            $col[1]=$coordonnees2;
                return new JsonResponse(['titre'=>'coucou','totalParticipation'=>'14','col'=> $col]);
    }


    
}
