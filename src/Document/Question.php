<?php
  namespace App\Document;
  use App\Document\Reponse;
  use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
  use Doctrine\Common\Collections\ArrayCollection;
 

  /**
   * @MongoDB\EmbeddedDocument
   * @MongoDB\Document(repositoryClass="App\Repository\QuestionRepository")
   */
  Class Question
  {
      /**
       * @MongoDB\Id(strategy="INCREMENT")
       */
          private $id;

       //permettra d'indexer les types des champs de selections
      //altMongo\EmbedOne(targetDocument=configurations) et une collection
        
       /**
        * @MongoDB\field(type="string")
        */
        private $typeReponse;
          
        /**
         * @MongoDB\Field(type="string")
         */
        private $message;
        /**
         * @MongoDB\EmbedMany(targetDocument="Reponse")
         */
        private $reponses = [];

        public function __construct()
        {
            $this->reponses = new ArrayCollection();
        }
        /**
         * @param Reponse $reponse
         */
        
        public function addReponse(Reponse $reponse)
        {
            $this->reponses->add($reponse);
        }
        public function removeReponses(Reponse $reponse)
        {
            $this->reponses->removeElements($reponse);
        }



        /**
         * Get the value of message
         */ 
        public function getMessage()
        {
                return $this->message;
        }

        /**
         * Set the value of message
         *
         * @return  self
         */ 
        public function setMessage($message)
        {
                $this->message = $message;

                return $this;
        }

        /**
         * Get the value of typeReponse
         */ 
        public function getTypeReponse()
        {
                return $this->typeReponse;
        }

        /**
         * Set the value of typeReponse
         *
         * @return  self
         */ 
        public function setTypeReponse($typeReponse)
        {
                $this->typeReponse = $typeReponse;

                return $this;
        }

        /**
         * Get the value of reponses
         */ 
        public function getReponses()
        {
                return $this->reponses;
        }
        public function setReponses(Array $responses)
        {
            foreach($responses as $reponse)
            {
                $this->reponses->add($reponse);
            }
  
        }
  }

?>
