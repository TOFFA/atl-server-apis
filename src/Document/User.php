<?php
   namespace App\Document;
   
   use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

    /**
     * @MongoDB\EmbeddedDocument
     */
    Class User 
     {
        /**
         * @MongoDB\Id(strategy="INCREMENT")
         */
          private $id;
          //Id(strategy="INCREMENT")
         
        /**
         * @MongoDB\Field(type="string")
         */
          private $firstName;
      
        /**
         * @MongoDB\Field(type="string")
         */
          private $lastName;
        /**
        * @MongoDB\Field(type="string")
        */
         private $numTel;

        /**
         * @MongoDB\Field(type="string")
         */
        private $email;
       
        /**
         * @MongoDB\Field(type="date")
         */
        private $dateNaissance;

        /**
         * @MongoDB\Field(type="string")
         */
        private $lieuNaissance;
        /**
         * @MongoDB\Field(type="string")
         */
        private $paysNaissance;
        /**
         * @MongoDB\Field(type="string")
         */
        private $photo;


        public function __construct()
        {
            
        }

          /**
           * Get the value of firstName
           */ 
          public function getFirstName()
          {
                    return $this->firstName;
          }

          /**
           * Set the value of firstName
           *
           * @return  self
           */ 
          public function setFirstName($firstName)
          {
                    $this->firstName = $firstName;

                    return $this;
          }

          /**
           * Set the value of lastName
           *
           * @return  self
           */ 
          public function setLastName($lastName)
          {
                    $this->lastName = $lastName;

                    return $this;
          }

         /**
          * Get the value of numTel
          */ 
         public function getNumTel()
         {
                  return $this->numTel;
         }

         /**
          * Set the value of numTel
          *
          * @return  self
          */ 
         public function setNumTel($numTel)
         {
                  $this->numTel = $numTel;

                  return $this;
         }

        /**
         * Get the value of email
         */ 
        public function getEmail()
        {
                return $this->email;
        }

        /**
         * Set the value of email
         *
         * @return  self
         */ 
        public function setEmail($email)
        {
                $this->email = $email;

                return $this;
        }

        /**
         * Get the value of dateNaissance
         */ 
        public function getDateNaissance()
        {
                return $this->dateNaissance;
        }

        /**
         * Set the value of dateNaissance
         *
         * @return  self
         */ 
        public function setDateNaissance($dateNaissance)
        {
                $this->dateNaissance = $dateNaissance;

                return $this;
        }

        /**
         * Get the value of lieuNaissance
         */ 
        public function getLieuNaissance()
        {
                return $this->lieuNaissance;
        }

        /**
         * Set the value of lieuNaissance
         *
         * @return  self
         */ 
        public function setLieuNaissance($lieuNaissance)
        {
                $this->lieuNaissance = $lieuNaissance;

                return $this;
        }

        /**
         * Get the value of paysNaissance
         */ 
        public function getPaysNaissance()
        {
                return $this->paysNaissance;
        }

        /**
         * Set the value of paysNaissance
         *
         * @return  self
         */ 
        public function setPaysNaissance($paysNaissance)
        {
                $this->paysNaissance = $paysNaissance;

                return $this;
        }

        /**
         * Get the value of photo
         */ 
        public function getPhoto()
        {
                return $this->photo;
        }
    

        /**
         * Set the value of photo
         *
         * @return  self
         */ 
        public function setPhoto($photo)
        {
                $this->photo = $photo;

                return $this;
        }

          /**
           * Get the value of lastName
           */ 
          public function getLastName()
          {
                    return $this->lastName;
          }
   }

?>
