<?php

   namespace App\Document;
   
   use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
   use ApiPlatform\Core\Annotation\ApiResource;
   use JMS\Serializer\Annotation as Serializer;
   use App\Document\Question;


   
  /**
   * 
   * @MongoDB\Document
   *
   */
     
   class Reponse{
       /**
        * @MongoDB\Id(strategy="INCREMENT",type="integer")
        */
        private $id;
        /**
         * @MongoDB\Field(type="string")
         * 
         */
         private $label; //les contenus des reponses
         /**
          *@MongoDB\ReferenceOne(targetDocument="Question")
          */
          private $question;
       



         public function setId($id)
         {
                 $this->id = $id;
 
                 return $this;
         }

         /**
          * Get the value of label
          */ 
         public function getLabel()
         {
                  return $this->label;
         }

         /**
          * Set the value of label
          *
          * @return  self
          */ 
         public function setLabel($label)
         {
                  $this->label = $label;

                  return $this;
         }
         public function __toString()
         {
             return $this->label;
         }

        

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        

          /**
           * Get *@MongoDB\ReferenceOne(targetDocument="Question")
           */ 
          public function getQuestion()
          {
                    return $this->question;
          }

          /**
           * Set *@MongoDB\ReferenceOne(targetDocument="Question")
           *
           * @return  self
           */ 
          public function setQuestion($question)
          {
                    $this->question = $question;

                    return $this;
          }
   }
?>