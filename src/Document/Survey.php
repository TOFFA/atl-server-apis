<?php
  namespace App\Document;
  use App\Document\User;
  use App\Documen\Account;
  use App\Document\Content;
  use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
    
    /**
     * @MongoDB\Document(repositoryClass="App\Repository\SurveyRepository")
     * @@MongoDB\InheritanceType("COLLECTION_PER_CLASS")
     */

    
   class Survey{
    
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
     protected $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $title;
    /**
     * @MongoDB\Field(type="date")
     */
    protected $datePublication;
    
    /**
     * @MongoDB\Field(type="int")
     */
    
    protected $participantsNumbers;
    /**
     * @MongoDB\EmbedOne(targetDocument="Content")
     */
    protected $content;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Account")
     */
    protected $author;
    
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
      $this->title = $title;

        return $this;
    }

    /**
     * Get the value of datePublication
     */ 
    public function getDatePublication()
    {
        return $this->datePublication;
    }

    /**
     * Set the value of datePublication
     *
     * @return  self
     */ 
    public function setDatePublication($datePublication)
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * Get the value of participantsNumbers
     */ 
    public function getParticipantsNumbers()
    {
        return $this->participantsNumbers;
    }

    /**
     * Set the value of participantsNumbers
     *
     * @return  self
     */ 
    public function setParticipantsNumbers($participantsNumbers)
    {
        $this->participantsNumbers = $participantsNumbers;

        return $this;
    }
   
    
    

    /**
     * Get the value of content
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */ 
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

     /**
      * Get the value of id
      */ 
     public function getId()
     {
          return $this->id;
     }

     /**
      * Set the value of id
      *
      * @return  self
      */ 
     public function setId($id)
     {
          $this->id = $id;

          return $this;
     }

    /**
     * Get the value of author
     */ 
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of author
     *
     * @return  self
     */ 
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }
   }

    

   ?>
