<?php
   namespace App\Document;
   use App\Document\User;
   use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
   use Symfony\Component\Security\Core\User\UserInterface;
 
     
  

     
    /**
     * @MongoDB\Document(repositoryClass="App\Repository\AccountRepository")
     */
    Class Account implements UserInterface, \Serializable
     { 
        /**
         * @MongoDB\Id(strategy="INCREMENT")
         */
          protected $id;
          
         
        /**
         * @MongoDB\Field(type="string")
         */
          protected $username;
         /**
          *@MongoDB\EmbedOne(targetDocument="App\Document\User")
          */
          protected $user;
        /**
         * @MongoDB\Field(type="string")
         */
         protected $password;
        /**
        * @MongoDB\Field(type="boolean")
        */
        protected $isActive;
        /**
         * @MongoDB\Field(type="string")
         */
        protected $accountEmail;

        public function __construct()
        {
            $this->isActive = true;
        }

        public function getUsername()
        {
            return $this->username;
        }
        public function getRoles()
        {
            return array('ROLE_USER');
        }
        public function eraseCredentials()
        {

        }
        public function getPassword()
        {
            return $this->password;
        }
        public function getUser()
        {
            return $this->user;
        }
       
        public function getSalt()
        {
            return null;
        }
        public function serialize()
        {
            return serialize([
                $this->id,
                $this->username,
                $this->password
            ]);
        }
        public function unserialize($serialized)
        {
            list (
                $this->id,
                $this->username,
                $this->password,
                // see section on salt below
                // $this->salt
            ) = unserialize($serialized);

        }
       



          /**
           * Set the value of username
           *
           * @return  self
           */ 
          public function setUsername($username)
          {
                    $this->username = $username;

                    return $this;
          }

         /**
          * Set the value of password
          *
          * @return  self
          */ 
         public function setPassword($password)
         {
                  $this->password = $password;

                  return $this;
         }

          
          
        

      

          /**
           * Set *@MongoDB\EmbedOne(targetDocument="App\Document\User")
           *
           * @return  self
           */ 
          public function setUser($user)
          {
                    $this->user = $user;

                    return $this;
          }

        /**
         * Get the value of accountEmail
         */ 
        public function getAccountEmail()
        {
                return $this->accountEmail;
        }

        /**
         * Set the value of accountEmail
         *
         * @return  self
         */ 
        public function setAccountEmail($accountEmail)
        {
                $this->accountEmail = $accountEmail;

                return $this;
        }
    }
?>
