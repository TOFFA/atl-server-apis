<?php 
    namespace App\Document;
    use App\Document\Account;
    use App\Document\Content;
    use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
    use Doctrine\Common\Collections\ArrayCollection;
    /**
     * @MongoDB\Document(repositoryClass="App\Repository\SurveyUserRepository")
     * 
     */

      class SurveyUser{

       /**
        * @MongoDB\Id(strategy="INCREMENT")
        */
        protected $id;

        /**
         * @MongoDB\EmbedMany(targetDocument="Reponse")
         */
        private $givenAnswers = [];
        /**
         * @MongoDB\ReferenceOne(targetDocument="Survey")
         */
        private $survey;
        /**
         * @MongoDB\ReferenceOne(targetDocument="Account")
         */
        private $user;

        public function __construct()
        {
          $this->givenAnswers = new ArrayCollection();
        }
       
        /**
         * Get the value of survey
         */ 
        public function getSurvey()
        {
                return $this->survey;
        }

        /**
         * Set the value of survey
         *
         * @return  self
         */ 
        public function setSurvey($survey)
        {
                $this->survey = $survey;

                return $this;
        }

        /**
         * Get the value of user
         */ 
        public function getUser()
        {
                return $this->user;
        }

        /**
         * Set the value of user
         *
         * @return  self
         */ 
        public function setUser($user)
        {
                $this->user = $user;

                return $this;
        }
        public function addAnswers(Reponse $answers)
        {
            $this->givenAnswers->add($answers);
        }

        public function removeAnswers(Reponse $answers)
        {
          $this->givenAnswers->removeElements($answers);
        }
        public function setAnswers(Array $answersGiven)
        {
            foreach($answersGiven as $answer)
            {
                $this->givenAnswers->add($answer);
            }
  
        }

        /**
         * Get the value of givenAnswers
         */ 
        public function getGivenAnswers()
        {
                return $this->givenAnswers;
        }

        /**
         * Set the value of givenAnswers
         *
         * @return  self
         */ 
        public function setGivenAnswers($givenAnswers)
        {
                $this->givenAnswers = $givenAnswers;

                return $this;
        }
    }

?>
