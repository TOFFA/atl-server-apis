<?php
  namespace App\Document;
  use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
  use Doctrine\Common\Collections\ArrayCollection;
  use App\Document\Question;
  /**
   * @MongoDB\Document(repositoryClass="App\Repository\ContentRepository")
   */
    class Content{

        /**
         * @MongoDB\Id(strategy="INCREMENT")
         */
        private $id;
        /**
         * @MongoDB\EmbedMany(targetDocument="Question")
         */
        private $questions = [];

        public function __construct()
        {
            $this->questions = new ArrayCollection();
        }

        /**
         * @param Question $question
         */
       public function addQuestions(Question $question)
        {
            $this->questions->add($question) ;
        }
        public function removeQuestions(Question $question)
        {
            $this->questions->removeElements($question);
        }
        public function setQuestions(ArrayCollection $questions)
        {
            foreach($questions as $question)
            {
                $this->questions->add($question);
            }

        }
       

        /**
         * Get the value of questions
         */ 
        public function getQuestions()
        {
                return $this->questions;
        }
    }


                                                             
?>
