<?php
    namespace App\Repository;
    //Classes pour effectuer les requetes doctrines
   use App\Document\Account;
   use Doctrine\ODM\MongoDB\DocumentRepository;
   use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
   use Symfony\Bridge\Doctrine\RegistryInterface;
    
   class SurveyUserRepository extends DocumentRepository
   {
       public function searchBySurvey($id)
       {
            return $this->createQueryBuilder('su')
                        ->join('su.survey','s')
                        ->addSelect('s')
                        ->andWhere('s.id = :id')
                        ->setParameter('id',$id)
                        ->getQuery()
                        ->getResult();
                    
       }
   }







?>