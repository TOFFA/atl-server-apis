<?php
   namespace App\Repository;
    //Classes pour effectuer les requetes doctrines
   use App\Document\Account;
   use Doctrine\ODM\MongoDB\DocumentRepository;
   use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
   use Symfony\Bridge\Doctrine\RegistryInterface;

   class AccountRepository extends DocumentRepository implements UserLoaderInterface  
   {
       public function loadUserByUsername($username)
       {
           return $this->createQueryBuilder('u')
                       ->find()
                       ->field('username')->equals($username)
                       ->getQuery()
                       ->execute();
       }

   }
   //https://blog.eleven-labs.com/fr/introduction-a-doctrine-query-builder-avec-mongodb/




?>
